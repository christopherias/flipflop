/*
* flipflop.js Version 1.1
*/

const   GRIDSIZE        = 6;        // Spielfeldgröße (inklusive Infofelder)
const   MIN_BOMBS       = 6;        // Minimum an Bomben
const   MAX_BOMBS       = 13;       // Maximum an Bomben
const   MIN_COIN        = 1;        // Minimum an Münzwert
const   MAX_COIN        = 3;        // Maximum an Münzwert
var     num_triple      = 0;        // Anzahl an 3x Münzen
var     num_double      = 0;        // Anzahl an 2x Münzen
var     score           = 1;        // Gesamtpunktzahl
var     game_finished   = false;    // überprüft, ob Spiel beendet


// initialsiere Spielfeld, wenn Dokument geladen wurde
window.onload = function() 
{
    create_gamegrid();
    place_bombs();
    place_multipliers();
    update_info();
    //testausgabe();
}

/*
* Erstellt Spielfeld mit Größe GRIDSIZE und zentriert es.
*/
function create_gamegrid()
{
    document.getElementById("status").innerHTML = "Status: Spiel läuft";
    document.getElementById("scoreboard").innerHTML = "Punkte: "+ score;
    var wrapper = document.getElementById("game-wrapper");
    var tbl     = document.createElement("table");
    tbl.id      = "spielfeld";

    for(var zeile = 0; zeile < GRIDSIZE; zeile++)
    {
        var tr = tbl.insertRow();

        for(var spalte = 0; spalte < GRIDSIZE; spalte++)
        {
            /*
            * Solange wir noch im Spielfeld sind, belege Zelle...
            */
            if ( (zeile < GRIDSIZE-1) && (spalte < GRIDSIZE-1) )
            {
                var td = tr.insertCell();
                td.dataset.value = "notset"; // setze Feldwert auf "notset" (== unbestimmt)
                td.dataset.marked = "false"; // Feld nicht markiert
                td.className = "feld";
                // fügt Eventhandler zu dem Feld, der ausgeführt wird, wenn der User auf das Feld klickt
                // benötigt Zwischenvariablen mit keyword let
                // siehe: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Closures#Creating_closures_in_loops_A_common_mistake
                let z = zeile;
                let s = spalte;
                // Eventhandler wird nach erstem Klick des Users gelöscht, um Mehrfachausführung zu verhindern
                td.addEventListener("click", function call_flip() { flip(z, s); this.removeEventListener("click", call_flip) });
                // Eventhandler zur Markierung des Feldes durch Spieler
                td.addEventListener("wheel", function call_mark() { mark_field(z, s); });
            }
            // ... ansonsten erstelle eine leere Zelle für die Infoboxen
            else
            {
                var td = tr.insertCell();
                td.dataset.value = "notset";
            }
        }
    }
    wrapper.appendChild(tbl);
}


/*
* Legt Bomben zufällig auf das Spielfeld
*/
function place_bombs()
{
    var num_bombs = get_randnum(MIN_BOMBS, MAX_BOMBS);
    var spielfeld = document.getElementById("spielfeld");
    var counter = 0;

    // solange noch nicht alle Bomben gelegt
    // rechne GRIDSIZE-2, da Index bei 0 beginnt und letzte Zeile/Spalte zu Infobox gehört
    while (counter < num_bombs)
    {
        var zeile = get_randnum(0, GRIDSIZE-2); // wähle Zeile von 1 bis GRIDSIZE-2
        var spalte = get_randnum(0, GRIDSIZE-2); // wähle Spalte von 1 bis GRIDSIZE-2
        var aktuelles_feld = spielfeld.rows[zeile].cells[spalte].dataset; 

        // wenn Feldwert noch nicht gesetzt
        if (aktuelles_feld.value === "notset")
        {
            aktuelles_feld.value = "0"; // setze Feldwert auf 0 (== Bombe);
            counter++; // erhöhe Anzahl gesetzter Bomben
        }
    }
}


/*
* Holt zufällige Zahl zwischen min und max (inklusiv)
*/
function get_randnum(min, max)
{
    min = Math.ceil(min);
    max = Math.floor(max);
    var result = Math.floor(Math.random() * (max - min +1)) + min;
    return result;
}


/*
* Füge Münzmultiplikatoren ein und speichert Anzahl von 3x und 2x Münzen
*/
function place_multipliers()
{
    var spielfeld = document.getElementById("spielfeld");

    for (var zeile = 0; zeile < GRIDSIZE-1; zeile++)
    {
        for (var spalte = 0; spalte < GRIDSIZE-1; spalte++)
        {
            var aktuelles_feld = spielfeld.rows[zeile].cells[spalte].dataset;
            // falls Feld noch nicht belegt, belege es mit zufälligem Münzwert
            if (aktuelles_feld.value === "notset")
            {
                var coin = get_randnum(MIN_COIN, MAX_COIN);
                aktuelles_feld.value = String(coin);

                // zähle Anzahl an 3x und 2x Münzen
                if (aktuelles_feld.value === "3")
                {
                    num_triple++;
                }
                else if (aktuelles_feld.value === "2")
                {
                    num_double++;
                }
            }
        }
    }
    //console.log("Anzahl 3x: " + num_triple + "; Anzahl 2x: " + num_double);
}


/*
* Fügt Infotext bzgl. Bombenanzahl und Münzsumme in Infozeile/Infospalte ein
*/
function update_info()
{
    var spielfeld = document.getElementById("spielfeld");

    // berechne Infotext für jede Zeile
    for (var zeile = 0; zeile < GRIDSIZE-1; zeile++)
    {
        var zeile_coin_sum  = 0;
        var zeile_bombs     = 0;

        for (var spalte = 0; spalte < GRIDSIZE-1; spalte++)
        {
            var aktuelles_feld  = spielfeld.rows[zeile].cells[spalte].dataset;

            // wenn == "0" -> Bombe auf dem Feld
            if (aktuelles_feld.value === "0")
            {
                zeile_bombs++;
            }
            // ansonsten erhöhe Münzsumme der Zeile
            else
            {
                zeile_coin_sum += Number(aktuelles_feld.value);
            }
        }

        // erstelle zwei p-Elemente, welche den Wert für die Anzahl Bomben, bzw. Münzsumme erhalten
        var coin_info = document.createElement("div");
        coin_info.className = "coin_info";
        coin_info.appendChild(document.createTextNode(zeile_coin_sum));
        var bomb_info = document.createElement("div");
        bomb_info.className = "bomb_info";
        bomb_info.appendChild(document.createTextNode(zeile_bombs));

        // füge zeile_coin_sum und zeile_bombs in p-Elemente ein
        // GRIDSIZE-1 für cells, um letztes Element in der Zeile auszuwählen (== Infobox)
        var info_zeile = spielfeld.rows[zeile].cells[GRIDSIZE-1]; 
        info_zeile.appendChild(coin_info);
        info_zeile.appendChild(bomb_info);
    }

    // berechne Infotext für jede Spalte
    for (var spalte = 0; spalte < GRIDSIZE-1; spalte++)
    {
        var spalte_coin_sum  = 0;
        var spalte_bombs     = 0;

        for (var zeile = 0; zeile < GRIDSIZE-1; zeile++)
        {
            var aktuelles_feld  = spielfeld.rows[zeile].cells[spalte].dataset;

            // wenn == "0" -> Bombe auf dem Feld
            if (aktuelles_feld.value === "0")
            {
                spalte_bombs++;
            }
            // ansonsten erhöhe Münzsumme der Zeile
            else
            {
                spalte_coin_sum += Number(aktuelles_feld.value);
            }
        }

        // erstelle zwei p-Elemente, welche den Wert für die Anzahl Bomben, bzw. Münzsumme erhalten
        var coin_info = document.createElement("div");
        coin_info.className = "coin_info";
        coin_info.appendChild(document.createTextNode(spalte_coin_sum));
        var bomb_info = document.createElement("div");
        bomb_info.className = "bomb_info";
        bomb_info.appendChild(document.createTextNode(spalte_bombs));

        // füge zeile_coin_sum und zeile_bombs in p-Elemente ein
        // GRIDSIZE-1 für rows, um letztes Element in der Spalte auszuwählen (== Infobox)
        var info_spalte = spielfeld.rows[GRIDSIZE-1].cells[spalte];
        info_spalte.appendChild(coin_info);
        info_spalte.appendChild(bomb_info);
    }
}


/*
* Gibt alle Werte in den Feldern per console aus 
*/
function testausgabe()
{
    var spielfeld = document.getElementById("spielfeld");

    for (var zeile = 0; zeile < GRIDSIZE; zeile++)
    {
        for (var spalte = 0; spalte < GRIDSIZE; spalte++)
        {
            var aktuelles_feld = spielfeld.rows[zeile].cells[spalte].dataset;
            console.log("Zeile: " + (zeile+1) + "; Spalte: " + (spalte+1) + "; Wert: " + aktuelles_feld.value);
        }
    }
}


/*
* Deckt Feld auf und ermittelt Ergebnis, wenn das Spiel noch nicht beendet wurde
*/
function flip(zeile, spalte)
{

        var spielfeld = document.getElementById("spielfeld");
        var aktuelles_feld = spielfeld.rows[zeile].cells[spalte];
        var wert = aktuelles_feld.dataset.value;

        aktuelles_feld.appendChild(document.createTextNode(wert));

        // zeige Wert des Feldes an und wertet es aus
        if (wert === "0")
        {
            aktuelles_feld.className = "flipped-bad";
            // überspringen, falls Spiel beendet
            if (game_finished != true)
            {
                gameover();
            }
        }
        else if (wert === "3")
        {
            aktuelles_feld.className = "flipped";
            num_triple--;
            // überspringen, falls Spiel beendet
            if (game_finished != true)
            {
                increase_score(3);
                check_victory();
            }

        }
        else if (wert === "2")
        {
            aktuelles_feld.className = "flipped";
            num_double--;
            // überspringen, falls Spiel beendet
            if (game_finished != true)
            {
                increase_score(2);
                check_victory();
            }
        }
        else
        {
            aktuelles_feld.className = "flipped";
        }
}


/*
* Prüft, ob Spiel beendet, indem alle 2er und 3er aufgedeckt worden
*/
function check_victory()
{
    if ( (num_triple === 0) && (num_double === 0) )
    {
        document.getElementById("status").innerHTML = "Gewonnen!";
        game_finished = true;
    }
}


/*
* Beendet Spiel mit einem Gameover
*/
function gameover()
{
    score *= 0;
    document.getElementById("status").innerHTML = "Game Over!";
    document.getElementById("scoreboard").innerHTML = "Punkte: 0";
    game_finished = true;
}


/*
* Erhöht Punktzahl um gegebenen Faktor
*/
function increase_score(num)
{
    score *= num;
    document.getElementById("scoreboard").innerHTML = "Punkte: " + score;
}


/*
* Markiert das vom User ausgewählte Feld; 
* falls das Feld bereits markiert ist, entfernt es die Markierung
*/
function mark_field(zeile, spalte)
{
    if ( game_finished != true ) // deaktivieren, wenn Spiel gewonnen/verloren
    {
        var spielfeld = document.getElementById("spielfeld");
        var aktuelles_feld = spielfeld.rows[zeile].cells[spalte];
        var wert = aktuelles_feld.dataset.marked;
    
        // falls Feld unmarkiert
        if (wert === "false")
        {
            aktuelles_feld.dataset.marked = "true";
            aktuelles_feld.classList.add("marked_field"); // füge dem Feld die CSS-Klasse "marked_field" hinzu
        }
        else 
        {
            aktuelles_feld.dataset.marked = "false";
            aktuelles_feld.classList.remove("marked_field"); // entferne Markierung
        }
    }
}

